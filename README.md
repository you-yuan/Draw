# Draw

[![java](https://img.shields.io/badge/language-java-orange.svg)]()
[![jdk](https://img.shields.io/badge/jdk-1.8-green.svg)]()
[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

* 作者：YouYuan  
* 邮箱：xiyousuiyuan#163.com  
* QQ：1265161633  

## 简介  

draw.io 是一款在线图表编辑工具, 支持设计工作流、BPM、org charts、UML、ER图、网络拓朴图等各类图表。

本项目是基于Draw.io修改的离线版本，可直接本地部署运行，通过Web即可制作精美的各类图表。 

## 展示图

![home](./releases/home.jpg)

> 已将Draw和Tomcat进行了集成，可直接下载/releases/tomcat-8-Draw.zip解压运行。
> 
> 默认访问地址 http://127.0.0.1:12620